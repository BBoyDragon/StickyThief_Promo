using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Starter : MonoBehaviour
{
   [SerializeField]
    private List<GameObject> _patrolSpots;
    [SerializeField]
    private List<GameObject> _patrolSpots1;
    [SerializeField]
    private List<GameObject> _patrolSpots2;
    [SerializeField]
    private List<GameObject> _patrolSpots3;
    [SerializeField]
    private List<GameObject> SpawnPoints;
    [SerializeField]
    private CharacterController Character;
    [SerializeField]
    private CharacterSubView SubView;
    [SerializeField]
    private Button Up;
    [SerializeField]
    private Button Down;
    [SerializeField]
    private Button Left;
    [SerializeField]
    private Button Right;
    [SerializeField]
    private Material WallMaterial;
    public List<GameObject> Walls;
    public List<GameObject> Floors;


    private AiEnemyController Enemy;
    private AiEnemyController Enemy1;
    private AiEnemyController Enemy2;
    private AiEnemyController Enemy3;

    void Start()
    {
        Character = new CharacterController(Up,Down,Right,Left);
        SubView = GameObject.FindObjectOfType<CharacterSubView>();
        BulletsDamageController.view = SubView;
        Enemy = new AiEnemyController(_patrolSpots,SpawnPoints[0]);
        Enemy1 = new AiEnemyController(_patrolSpots1, SpawnPoints[1]);
        Enemy2 = new AiEnemyController(_patrolSpots2, SpawnPoints[2]);
        Enemy3 = new AiEnemyController(_patrolSpots3, SpawnPoints[3]);
        BulletsDamageController.Enemylist.Add(Enemy);
        BulletsDamageController.Enemylist.Add(Enemy1);
        BulletsDamageController.Enemylist.Add(Enemy3);
        BulletsDamageController.Enemylist.Add(Enemy2);
        BulletsDamageController.ClearPool();

        Walls.AddRange(GameObject.FindGameObjectsWithTag("Wall"));
        Floors.AddRange(GameObject.FindGameObjectsWithTag("Flor"));
        float r = Random.Range(0f, 1f);
        float g = Random.Range(0f, 1f);
        float b = Random.Range(0f, 1f);

        float r2 = Random.Range(0f, 1f);
        float g2 = Random.Range(0f, 1f);
        float b2 = Random.Range(0f, 1f);

        foreach (GameObject wall in Walls)
        {
            MeshRenderer renderer = wall.GetComponent<MeshRenderer>();
            Color col = new Color(r, g, b);
            renderer.material.SetColor("_Color", col);
        }
        foreach (GameObject flor in Floors)
        {
            MeshRenderer renderer = flor.GetComponent<MeshRenderer>();
            Color col = new Color(r2, g2, b2);
            renderer.material.SetColor("_Color", col);
        }
    }
    private void Update()
    {
        Character.Execute();
        Enemy.Execute();
        Enemy1.Execute();
        Enemy2.Execute();
        Enemy3.Execute();
    }

}
