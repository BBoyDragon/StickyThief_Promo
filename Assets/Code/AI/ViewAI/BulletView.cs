using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletView : MonoBehaviour
{
    [SerializeField]
    private Vector3 _finalPoint;
    [SerializeField]
    private float _bulletSpeed;

    public event Action<BulletView> WasTouched = delegate (BulletView view) { };
    public event Action CharacterDetected = delegate () { };

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            CharacterDetected.Invoke();
            WasTouched.Invoke(this);
        }
        else
        {
            WasTouched.Invoke(this);
        }
    }

    public Vector3 FinalPoint { get => _finalPoint; set => _finalPoint = value; }
    public float BulletSpeed { get => _bulletSpeed; set => _bulletSpeed = value; }
}
