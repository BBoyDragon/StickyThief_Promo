using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;
using System;

public class AiEnemyView : MonoBehaviour
{
    [SerializeField]
    private NavMeshAgent _navMeshAgent;

    [SerializeField]
    private float _speed;

    [SerializeField]
    private bool _isStopingBeforeNextPoint;

    [SerializeField]
    private float _nextPointTimeDelation;

    [SerializeField]
    private List<GameObject> _patrolSpots;

    [SerializeField]
    private Animator _enemyAnimator;

    [SerializeField]
    private GameObject _bulletSpawnPoint;

    [SerializeField]
    private Transform _firstGunPoint;
    [SerializeField]
    private Transform _secondGunPoint;
    [SerializeField]
    private GameObject _gun;


    public event Action PlayerWasDetected = delegate () { };
    public event Action PlayerWasLost = delegate () { };


    public List<GameObject> PatrolSpots { get => _patrolSpots; set => _patrolSpots = value; }
    public NavMeshAgent NavMeshAgent { get => _navMeshAgent; set => _navMeshAgent = value; }
    public float Speed { get => _speed; set => _speed = value; }
    public Animator EnemyAnimator { get => _enemyAnimator; set => _enemyAnimator = value; }
    public bool IsStopingBeforeNextPoint { get => _isStopingBeforeNextPoint; set => _isStopingBeforeNextPoint = value; }
    public float NextPointTimeDelation { get => _nextPointTimeDelation; set => _nextPointTimeDelation = value; }
    public GameObject BulletSpawnPoint { get => _bulletSpawnPoint; set => _bulletSpawnPoint = value; }
    public Transform FirstGunPoint { get => _firstGunPoint; set => _firstGunPoint = value; }
    public Transform SecondGunPoint { get => _secondGunPoint; set => _secondGunPoint = value; }
    public GameObject Gun { get => _gun; set => _gun = value; }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerWasDetected.Invoke();
        }
    }
}
