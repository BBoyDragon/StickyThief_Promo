using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootingSystemModel 
{   
    private AiEnemyView Enemy;
    private List<BulletView> BulletsOnTheWay;
    public EnemyShootingSystemModel(AiEnemyView enemy)
    {
        BulletsOnTheWay = new List<BulletView>();
        Enemy = enemy;
    }
    public void Shoot()
    {
        BulletView curentBullet = BulletsDamageController.BulletPool.Pop(Enemy.BulletSpawnPoint.transform);
        curentBullet.CharacterDetected += BulletsDamageController.DealDamage;
        curentBullet.WasTouched += Destroing;
        BulletsOnTheWay.Add(curentBullet);
        curentBullet.FinalPoint = new Vector3(GameObject.FindGameObjectWithTag("Player").transform.position.x - Enemy.BulletSpawnPoint.transform.position.x,
                                                                            0,
                                                                            GameObject.FindGameObjectWithTag("Player").transform.position.z - Enemy.BulletSpawnPoint.transform.position.z).normalized;
        Couratine = DestroyBulletTimer(curentBullet);
        BulletsOnTheWay[BulletsOnTheWay.Count - 1].StartCoroutine(Couratine);

    }
    private IEnumerator Couratine;
    public void BulletTransition()
    {
        if (BulletsOnTheWay.Count != 0)
        {
            foreach (BulletView bullet in BulletsOnTheWay)
            {
                bullet.transform.Translate(bullet.FinalPoint.normalized  * bullet.BulletSpeed*Time.deltaTime);
            }
        }
    }
    public  IEnumerator DestroyBulletTimer(BulletView curentB)
    {

        yield return new WaitForSeconds(5);
        Destroing(curentB);

    }
    private void Destroing(BulletView curentBullet)
    {
        curentBullet.CharacterDetected -= BulletsDamageController.DealDamage;
        curentBullet.WasTouched -= Destroing;
        BulletsOnTheWay.Remove(curentBullet);
        BulletsDamageController.BulletPool.Push(curentBullet);
    }

}
