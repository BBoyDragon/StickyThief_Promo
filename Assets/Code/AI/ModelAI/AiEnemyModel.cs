using JoostenProductions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AiEnemyModel
{
    private AiEnemyView View;
    private GameObject PlView;
    private float StartingDelation;
    private float TimerPoint;
    private bool IsCouratineActivated=false;
    private float MaxTimeBeforeShoot=1;
    private float CurentTimeBeforeShoot=2;

    private EnemyShootingSystemModel EnemyShootingSystem;
    public event Action TooLongDistance = delegate () { };
    public AiEnemyModel(AiEnemyView _viev,GameObject playerView)
    {
        TimerPoint = Time.deltaTime;
        View = _viev;
        View.NavMeshAgent.speed = View.Speed;
        PlView = playerView;
        StartingDelation = View.NextPointTimeDelation;
        EnemyShootingSystem = new EnemyShootingSystemModel( View);
    }
    public void Execute()
    {
        CheckDestinationBehavour();
        EnemyShootingSystem.BulletTransition();
    }
    public void AttackExecute()
    {
        StartAttackingBehavour();
    }
    public void OnStateChangeBehavour(EnemyStates enemyStates )
    {
        switch (enemyStates)
        {
            case EnemyStates.Patrol:
               // UpdateManager.UnsubscribeFromUpdate(StartAttackingBehavour);
                //UpdateManager.UnsubscribeFromUpdate(EnemyShootingSystem.BulletTransition);
                View.NavMeshAgent.stoppingDistance = 2;
                View.NavMeshAgent.SetDestination(View.PatrolSpots[0].transform.position);
               // UpdateManager.SubscribeToUpdate(CheckDestinationBehavour);

                break;
            case EnemyStates.Attack:
               // UpdateManager.SubscribeToUpdate(EnemyShootingSystem.BulletTransition);
              //  UpdateManager.SubscribeToUpdate(CheckDestinationBehavour);
                View.NavMeshAgent.stoppingDistance = 12;
                //UpdateManager.SubscribeToUpdate (StartAttackingBehavour);
                //View.NavMeshAgent.isStopped = true;

                break;
        }
    }
    private void CheckDestinationBehavour()
    {
        if (View.NavMeshAgent.remainingDistance <= View.NavMeshAgent.stoppingDistance)
        {
            View.EnemyAnimator.SetBool("isWalking", false);
            View.Gun.transform.position = View.SecondGunPoint.position; // �������
            View.Gun.transform.rotation = View.SecondGunPoint.rotation;
        }
        else
        {
            View.EnemyAnimator.SetBool("isWalking", true);
            View.Gun.transform.position = View.FirstGunPoint.position;//�������
            View.Gun.transform.rotation = View.FirstGunPoint.rotation;
        }

        if (View.NavMeshAgent.remainingDistance < 2f) 
        {
            TimeDelation();
        }
    }
    private void StartAttackingBehavour()
    {
        Quaternion rotation = Quaternion.LookRotation(-(View.transform.position-PlView.transform.position));
        rotation.x = 0f;
        rotation.z = 0f;
        View.transform.rotation = Quaternion.Slerp(View.transform.rotation, rotation, Time.deltaTime * 8);
        View.NavMeshAgent.SetDestination(PlView.transform.position);
        if (View.NavMeshAgent.remainingDistance > 60)
        {
            TooLongDistance.Invoke();
        }
        if (View.NavMeshAgent.remainingDistance <= View.NavMeshAgent.stoppingDistance)
        {
            View.EnemyAnimator.SetBool("isWalking", false);
            if (CurentTimeBeforeShoot >= MaxTimeBeforeShoot)
            {
                EnemyShootingSystem.Shoot();
                CurentTimeBeforeShoot = 0;
            }
            else
            {
                CurentTimeBeforeShoot +=Time.deltaTime ;
            }
        }
        else
        {
            View.EnemyAnimator.SetBool("isWalking", true);
        }
    }

    private Vector3 Vector3(int v1, int v2, int v3)
    {
        throw new NotImplementedException();
    }

    private void NewPointChecker()
    {
        View.NavMeshAgent.SetDestination(View.PatrolSpots[UnityEngine.Random.Range(0, View.PatrolSpots.Count)].transform.position);
    }
    private void TimeDelation()
    {
        if (View.IsStopingBeforeNextPoint)
        {
            if (View.NextPointTimeDelation > 0)
            {
                View.NextPointTimeDelation -= TimerPoint;
                //Debug.Log(View.NextPointTimeDelation);
            }
            else
            {
                View.NextPointTimeDelation = StartingDelation;
                NewPointChecker();
            }
        }
        else
        {
            NewPointChecker();
        }
    }
    public void Clear()
    {
        //UpdateManager.UnsubscribeFromUpdate(EnemyShootingSystem.BulletTransition);
        //UpdateManager.UnsubscribeFromUpdate(StartAttackingBehavour);
        //UpdateManager.UnsubscribeFromUpdate(CheckDestinationBehavour);
    }

}
