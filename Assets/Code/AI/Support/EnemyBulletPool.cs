using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletPool
{
    private readonly Stack<BulletView > _stack;
    public EnemyBulletPool()
    {
        _stack = new Stack<BulletView>();
        _stack.Clear();
    }

    public void Push(BulletView go)
    {
        _stack.Push(go);
        go.gameObject.SetActive(false);
    }

    public BulletView Pop(Transform PopingPosition)
    {
        BulletView go;
        if (_stack.Count == 0)
        {
            go = GameObject.Instantiate<BulletView>(Resources.Load<BulletView>("EnemyBullet"), PopingPosition.position, Quaternion.identity);
            go.CharacterDetected += BulletsDamageController.DealDamage;
        }
        else
        {
            go = _stack.Pop();
        }
        go.gameObject.SetActive(true);
        go.transform.position = PopingPosition.position;
        return go;
    }
    public void ClearPool()
    {
        _stack.Clear();
    }
}
