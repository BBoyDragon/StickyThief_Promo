using Tools;
using UnityEngine;
using System.Collections.Generic;


public class AiEnemyController :BaseController
{
    private Transform SpawnPoint;
    private AiEnemyView _aiEnemyView;
    private AiEnemyModel _aiEnemyModel;
    private SubscriptionProperty<EnemyStates> _enemyStates;
    private GameObject _playerView;
    public AiEnemyController(List<GameObject> spots,GameObject spawnPoint)
    {
        SpawnPoint = spawnPoint.transform;
        _playerView = GameObject.FindGameObjectWithTag("Player");
        InicializeView(spots);
        _aiEnemyModel = new AiEnemyModel(_aiEnemyView,_playerView);
        _enemyStates = new SubscriptionProperty<EnemyStates>();
        _enemyStates.SubscribeOnChange(_aiEnemyModel.OnStateChangeBehavour);
        _aiEnemyModel.TooLongDistance += StartPatroling;
        StartPatroling();
    }
    private void StartAtacking()
    {
        _enemyStates.Value = EnemyStates.Attack;
    }
    private void StartPatroling()
    {
        _enemyStates.Value = EnemyStates.Patrol;
    }
    private void InicializeView(List<GameObject> _spots)
    {
        _aiEnemyView = GameObject.Instantiate<AiEnemyView>(Resources.Load<AiEnemyView>("enemy3"),SpawnPoint.position,Quaternion.identity);
        _aiEnemyView.PatrolSpots = _spots;
        AddGameObjects(_aiEnemyView.gameObject);
        _aiEnemyView.PlayerWasDetected += StartAtacking;
    }
    public void Deactivate()
    {
        _aiEnemyModel.Clear();
    }
    public void Execute()
    {
        _aiEnemyModel.Execute();
        if (_enemyStates.Value  == EnemyStates.Attack)
        {
            _aiEnemyModel.AttackExecute();
        }
    }
}

public enum EnemyStates
{
    Patrol,
    Attack
}
