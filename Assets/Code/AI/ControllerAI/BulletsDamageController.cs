using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BulletsDamageController 
{
    public static CharacterSubView view;
    public static EnemyBulletPool BulletPool = new EnemyBulletPool();
    public static List<AiEnemyController> Enemylist = new List<AiEnemyController>();
    public static void DealDamage()
    {
        view.Hp -= 20;
    }
    public static void ClearPool()
    {
        BulletPool.ClearPool();
    }
}
