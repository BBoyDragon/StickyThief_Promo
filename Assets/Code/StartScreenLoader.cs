using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartScreenLoader : MonoBehaviour
{
    [SerializeField]
    private Button StartButton;
    public void Start()
    {
        StartButton.onClick.AddListener(Load);
    }
    public void Load()
    {
        SceneManager.LoadScene(Random.Range(1, 3));

    }

}
