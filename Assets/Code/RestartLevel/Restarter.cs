using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Restarter : MonoBehaviour
{
    [SerializeField]
    private Button RestartButton;
    private void Start()
    {
        RestartButton.onClick.AddListener(Restart);
    }
    private void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
