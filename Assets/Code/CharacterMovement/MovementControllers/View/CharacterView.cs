using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterView : MonoBehaviour
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private CheckerView _UpChecker;
    [SerializeField]
    private CheckerView _DownChecker;
    [SerializeField]
    private CheckerView _LeftChecker;
    [SerializeField]
    private CheckerView _RightChecker;
    [SerializeField]
    private CheckerView _ForwardChecker;
    [SerializeField]
    private CheckerView _BackChecker;



    public float Speed { get => speed; set => speed = value; }
    public CheckerView UpChecker { get => _UpChecker; set => _UpChecker = value; }
    public CheckerView DownChecker { get => _DownChecker; set => _DownChecker = value; }
    public CheckerView LeftChecker { get => _LeftChecker; set => _LeftChecker = value; }
    public CheckerView RightChecker { get => _RightChecker; set => _RightChecker = value; }
    public CheckerView ForwardChecker { get => _ForwardChecker; set => _ForwardChecker = value; }
    public CheckerView BackChecker { get => _BackChecker; set => _BackChecker = value; }
}
