using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSubView : MonoBehaviour
{
    [SerializeField]
    private Animator animator;
    [SerializeField ]
    private float maxHP;
    [SerializeField]
    private float hp;

    public event Action WasKilled = delegate () { };
    public Animator AnimatorView { get => animator; set => animator = value; }
    public float Hp
    {
        get => hp; set
        {
            if ((value) <= 0)
            {
                WasKilled.Invoke();
                Debug.Log("fyfy");
                hp = maxHP;
            }
            else
            {
                hp = value;
            }
        }
    }
}
