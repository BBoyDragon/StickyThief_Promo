using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelGeneratorView : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "sub")
        {
            SceneManager.LoadScene(Random.Range(1,3));
        }
    }
}
