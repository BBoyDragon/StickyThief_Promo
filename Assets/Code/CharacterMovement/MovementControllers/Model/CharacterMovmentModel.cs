using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovmentModel 
{
    private CharacterView characterView;
    private InputController inputController;
    private Vector3 _ForwardBackVector;
    private Vector3 _LeftRightVector;
    private CharacterSubView characterSubView;
    public CharacterMovmentModel(InputController _inputController,CharacterView _characterView ,CharacterSubView _characterSubView )
    {
        inputController = _inputController;
        characterView = _characterView;
        characterSubView = _characterSubView;
        ForwardBackVector =Vector3.forward;
        LeftRightVector = Vector3.right;
    }

    public Vector3 ForwardBackVector { get => _ForwardBackVector; set => _ForwardBackVector = value; }
    public Vector3 LeftRightVector { get => _LeftRightVector; set => _LeftRightVector = value; }

    public void Execute()
    {
        
         characterView.transform.Translate(-ForwardBackVector *  inputController.UpCof * characterView.Speed * Time.deltaTime);

         characterView.transform.Translate(-ForwardBackVector   * inputController.DownCof   * characterView.Speed * Time.deltaTime);

        characterView.transform.Translate(-LeftRightVector * inputController.LeftCof * characterView.Speed * Time.deltaTime);

        characterView.transform.Translate(-LeftRightVector * inputController.RightCof * characterView.Speed * Time.deltaTime);

        if (Vector3.Distance(characterSubView.transform.position, characterView.RightChecker.transform.position) > 0.1f)
        {
            characterSubView.AnimatorView.SetBool("IsStanding", false);
            characterSubView.transform.position = Vector3.MoveTowards(characterSubView.transform.position, characterView.RightChecker.transform.position, characterView.Speed * Time.deltaTime);
        }
        else
        {
            characterSubView.AnimatorView.SetBool("IsStanding", true);
        }

        characterSubView.transform.LookAt(characterView.RightChecker.transform);
    }
}
