using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InputController :BaseController
{
    public Button UpButtonUi;
    public Button RightButtonUi;
    public Button LeftButtonUi;
    public Button DownButtonUi;
    public int UpCof;
    public int RightCof;
    public int LeftCof;
    public int DownCof;
    public bool IsUpButtonPressed
    {
        get 
        {
            if (Input.GetKey(KeyCode.W))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public bool IsDownButtonPressed
    {
        get
        {
            if (Input.GetKey(KeyCode.S))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public bool IsRightButtonPressed
    {
        get
        {
            if (Input.GetKey(KeyCode.D))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public bool IsLeftButtonPressed
    {
        get
        {
            if (Input.GetKey(KeyCode.A ))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }


    private string UpButton;
    private string DownButton;
    private string RightButton;
    private string LeftButton;


    public InputController(string UpButton,string DownButton,string LeftButton, string RightButton, Button Up,Button Right,Button Left,Button Down)
    {
        UpButton = this.UpButton;
        DownButton = this.DownButton;
        LeftButton = this.LeftButton;
        RightButton = this.RightButton;

        UpButtonUi = Up;
        EventTrigger Uptrigger = UpButtonUi.GetComponent<EventTrigger>();

        EventTrigger.Entry upentry = new EventTrigger.Entry();
        upentry.eventID = EventTriggerType.PointerDown;
        upentry.callback.AddListener((data)=> { UpCof = 1;});
        Uptrigger.triggers.Add(upentry);

        EventTrigger.Entry UpFreeEntry = new EventTrigger.Entry();
        UpFreeEntry .eventID = EventTriggerType.PointerExit;
        UpFreeEntry.callback.AddListener((data) => { UpCof = 0;});
        Uptrigger.triggers.Add(UpFreeEntry);

        DownButtonUi = Down;
        EventTrigger Downtrigger = DownButtonUi.GetComponent<EventTrigger>();

        EventTrigger.Entry DownEntry = new EventTrigger.Entry();
        DownEntry.eventID = EventTriggerType.PointerDown;
        DownEntry.callback.AddListener((data1) => { DownCof = -1;Debug.Log(DownCof); });
        Downtrigger.triggers.Add(DownEntry);

        EventTrigger.Entry DownFreeEntry = new EventTrigger.Entry();
        DownFreeEntry.eventID = EventTriggerType.PointerExit;
        DownFreeEntry.callback.AddListener((data1) => { DownCof = 0; Debug.Log(DownCof); });
        Downtrigger.triggers.Add(DownFreeEntry);

        RightButtonUi  = Right;
        EventTrigger Righttrigger = RightButtonUi.GetComponent<EventTrigger>();

        EventTrigger.Entry RightEntry = new EventTrigger.Entry();
        RightEntry.eventID = EventTriggerType.PointerDown;
        RightEntry.callback.AddListener((data1) => { RightCof = 1; Debug.Log(DownCof); });
        Righttrigger.triggers.Add(RightEntry);

        EventTrigger.Entry RightFreeEntry = new EventTrigger.Entry();
        RightFreeEntry.eventID = EventTriggerType.PointerExit;
        RightFreeEntry.callback.AddListener((data1) => { RightCof = 0; Debug.Log(DownCof); });
        Righttrigger.triggers.Add(RightFreeEntry);

        LeftButtonUi = Left;
        EventTrigger Lefttrigger = LeftButtonUi.GetComponent<EventTrigger>();

        EventTrigger.Entry LeftEntry = new EventTrigger.Entry();
        LeftEntry.eventID = EventTriggerType.PointerDown;
        LeftEntry.callback.AddListener((data1) => { LeftCof = -1; Debug.Log(DownCof); });
        Lefttrigger.triggers.Add(LeftEntry);

        EventTrigger.Entry LeftFreeEntry = new EventTrigger.Entry();
        LeftFreeEntry.eventID = EventTriggerType.PointerExit;
        LeftFreeEntry.callback.AddListener((data1) => { LeftCof = 0; Debug.Log(DownCof); });
        Lefttrigger.triggers.Add(LeftFreeEntry);
    }

    public float HorizontalAxis
    {
        get { return Input.GetAxis("Horizontal"); }
    }
    public float VerticalAxis
    {
        get { return Input.GetAxis("Vertical"); }
    }
    //public void UpOnClick()
    //{
    //    UpDownCof = 1;
    //}
    //public void UpOnFree()
    //{
    //    UpDownCof = 0;
    //}
    //public void RightOnClick()
    //{
    //    RightLeftCof = 1;
    //}
}
