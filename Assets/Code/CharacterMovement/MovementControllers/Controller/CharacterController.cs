using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JoostenProductions;
using UnityEngine.SceneManagement;

public class CharacterController :BaseController
{
    private CharacterView characterView;
    private InputController inputController;
    private CharacterMovmentModel movmentModel;
    private CharacterSubView characterSubView;
    public CharacterController(Button up, Button down, Button right, Button left)
    {
        characterView = GameObject.Instantiate<CharacterView>(Resources.Load<CharacterView>("Player"));
        characterSubView = GameObject.Instantiate<CharacterSubView>(Resources.Load<CharacterSubView>("SubView"));
        inputController = new InputController("W","S","A","D",up,right,left,down);
        movmentModel = new CharacterMovmentModel(inputController, characterView,characterSubView );
       // UpdateManager.SubscribeToUpdate(movmentModel.Execute);
        characterSubView.WasKilled += Death;
    }
    public void Execute()
    {
        movmentModel.Execute();
    }
    private void Death()
    {
        characterSubView.WasKilled -= Death;
        //UpdateManager.UnsubscribeFromUpdate(movmentModel.Execute);
        //BulletsDamageController.ClearSceneForRestart();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name) ;
    }
}
