using UnityEngine;
using System;

public class PlayerMovement : MonoBehaviour
{
	//����� ���� �� ��������� ������ ���� ����

	public SingleLimbParametrs LeftHand;
	public SingleLimbParametrs RightHand;
	public SingleLimbParametrs LeftFoot;
	public SingleLimbParametrs RightFoot;


	public Transform Pole;

	public float LenghtOfStep;

	public int Iterations;

	public float Delta;

	private SingleLimb _leftHand;
	private SingleLimb _rightHand;
	private SingleLimb _leftFoot;
	private SingleLimb _rightFoot;

	private void Start()
	{
		_leftHand = new SingleLimb();
		SetSimpleLimbParametrs(_leftHand, LeftHand);
		Init(_leftHand);

		_rightHand = new SingleLimb();
		SetSimpleLimbParametrs(_rightHand, RightHand);
		Init(_rightHand);

		_leftFoot = new SingleLimb();
		SetSimpleLimbParametrs(_leftFoot, LeftFoot);
		Init(_leftFoot);

		_rightFoot = new SingleLimb();
		SetSimpleLimbParametrs(_rightFoot, RightFoot);
		Init(_rightFoot);
	}
	private void SetSimpleLimbParametrs(SingleLimb limb, SingleLimbParametrs limbParametrs)
	{
		limb.BonesAmount = limbParametrs.BonesAmount;
		limb.LimbTransform = limbParametrs.Limb;
		limb.LookAtTransform = limbParametrs.LookAtTransform;
		limb.Raycaster = limbParametrs.Raycaster;
	}
	private void FixedUpdate()
	{
		BendLimb(_leftHand);
		BendLimb(_rightHand);
		Debug.DrawRay(_leftHand.Raycaster.position, Vector3.forward, Color.red);
		Debug.DrawRay(_rightHand.Raycaster.position, Vector3.forward, Color.red);
		CheckForPointDistance(_leftHand);
		CheckForPointDistance(_rightHand);

		BendLimb(_leftFoot);
		BendLimb(_rightFoot);

		CheckForPointDistance(_leftFoot);
		CheckForPointDistance(_rightFoot);
	}

	private void Init(SingleLimb limb)
	{
		limb.Bones = new Transform[limb.BonesAmount + 1];
		limb.Positions = new Vector3[limb.BonesAmount + 1];
		limb.BonesLenght = new float[limb.BonesAmount];
		limb.StartDirectionSucc = new Vector3[limb.BonesAmount + 1];
		limb.StartRotationBone = new Quaternion[limb.BonesAmount + 1];

		limb.StartRotationTarget = limb.LookAtTransform.rotation;

		limb.MaxLenght = 0;

		//init data

		var current = limb.LimbTransform;

		for (int i = limb.Bones.Length - 1; i >= 0; i--)
		{
			limb.Bones[i] = current;
			limb.StartRotationBone[i] = current.rotation;
			if (i == limb.Bones.Length - 1)
			{
				limb.StartDirectionSucc[i] = limb.LookAtTransform.position - current.position;
			}
			else
			{
				limb.StartDirectionSucc[i] = limb.Bones[i + 1].position - current.position;
				limb.BonesLenght[i] = (limb.Bones[i + 1].position - current.position).magnitude;
				limb.MaxLenght += limb.BonesLenght[i];
			}
			//limb.MaxLenght += 2f;
			current = current.parent;
		}
	}

	private void BendLimb(SingleLimb limb)
	{
		if (limb.LookAtTransform == null)
			return;
		if (limb.BonesLenght.Length != limb.BonesAmount)
			Init(limb);
		for (int i = 0; i < limb.Bones.Length; i++)
		{
			limb.Positions[i] = limb.Bones[i].position;
		}

		var RootRot = (limb.Bones[0].parent != null) ? limb.Bones[0].parent.rotation : Quaternion.identity;
		var RootRotDiff = RootRot * Quaternion.Inverse(limb.StartRotationRoot);

		//calculations
		if ((limb.LookAtTransform.position - limb.Bones[0].position).sqrMagnitude >= limb.MaxLenght * limb.MaxLenght)
		{
			var direction = (limb.LookAtTransform.position - limb.Bones[0].position).normalized;
			for (int i = 1; i < limb.Positions.Length; i++)
			{
				limb.Positions[i] = limb.Positions[i - 1] + direction * limb.BonesLenght[i - 1];
			}
		}
		else
		{
			for (int iteraton = 0; iteraton < Iterations; iteraton++)
			{
				//back
				for (int i = limb.Positions.Length - 1; i > 0; i--)
				{
					if ((i == limb.Positions.Length - 1))
						limb.Positions[i] = limb.LookAtTransform.position;
					else
						limb.Positions[i] = limb.Positions[i + 1] + (limb.Positions[i] - limb.Positions[i + 1]).normalized * limb.BonesLenght[i];
				}

				for (int i = 1; i < limb.Positions.Length; i++)
				{
					limb.Positions[i] = limb.Positions[i - 1] + (limb.Positions[i] - limb.Positions[i - 1]).normalized * limb.BonesLenght[i - 1];
				}

				//move close enough
				if ((limb.Positions[limb.Positions.Length - 1] - limb.LookAtTransform.position).sqrMagnitude < Delta * Delta)
					break;
			}
		}

		if (Pole != null)
		{
			for (int i = 1; i < limb.Positions.Length - 1; i++)
			{
				var plane = new Plane(limb.Positions[i + 1] - limb.Positions[i - 1], limb.Positions[i - 1]);
				var projectedPole = plane.ClosestPointOnPlane(Pole.position);
				var projectedBone = plane.ClosestPointOnPlane(limb.Positions[i]);
				var angle = Vector3.SignedAngle(projectedBone - limb.Positions[i - 1], projectedPole - limb.Positions[i - 1], plane.normal);
				limb.Positions[i] = Quaternion.AngleAxis(angle, plane.normal) * (limb.Positions[i] - limb.Positions[i - 1]) + limb.Positions[i + 1];
			}
		}

		for (int i = 0; i < limb.Positions.Length; i++)
		{
			if ((i == limb.Positions.Length - 1))
			{
				limb.Bones[i].rotation = limb.LookAtTransform.rotation * Quaternion.Inverse(limb.StartRotationTarget) * limb.StartRotationBone[i];
			}
			else
			{
				limb.Bones[i].rotation = Quaternion.FromToRotation(limb.StartDirectionSucc[i], limb.Positions[i + 1] - limb.Positions[i]) * limb.StartRotationBone[i];
			}
			limb.Bones[i].position = limb.Positions[i];
		}
	}

	private void CheckForPointDistance(SingleLimb limb)
	{
		RaycastHit hit;
		Ray ray = new Ray(limb.Raycaster.position, Vector3.forward);
		if (Physics.Raycast(ray, out hit, 1f))
		{
			if (Vector3.Distance(hit.point, limb.LookAtTransform.position) > LenghtOfStep)
			{
				limb.LookAtTransform.position = hit.point;
			}
			Debug.Log("Vector3.Distance(hit.point, limb.LookAtTransform.position) = " + Vector3.Distance(hit.point, limb.LookAtTransform.position));
		}
	}
}
[Serializable]
public class SingleLimb
{
	public Transform LimbTransform;
	public Transform LookAtTransform;
	public Transform Raycaster;
	public Transform[] Bones;
	public Quaternion[] StartRotationBone;
	public Quaternion StartRotationTarget;
	public Quaternion StartRotationRoot;
	public Vector3[] Positions;
	public Vector3[] StartDirectionSucc;
	public Vector3 UperarmLookAtVector;
	public float[] BonesLenght;
	public float MaxLenght;
	public float CurrentDistance;
	public int BonesAmount;
}

[Serializable]
public class SingleLimbParametrs
{
	public Transform Limb;
	public Transform LookAtTransform;
	public Transform Raycaster;
	public int BonesAmount;
}
